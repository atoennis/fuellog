package com.atoennis.fuellog;

import java.util.Date;
import java.util.GregorianCalendar;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.atoennis.fuellog.domain.Trip;
import com.atoennis.fuellog.domain.Vehicle;

public class FuelLogContentProviderTest extends ProviderTestCase2<FuelLogContentProvider>
{
    private MockContentResolver mockResolver;

    public FuelLogContentProviderTest()
    {
        super(FuelLogContentProvider.class, FuelTripContract.AUTHORITY);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        mockResolver = getMockContentResolver();
    }

    private void setUpTripData()
    {
        Trip t1 = new Trip(1, new GregorianCalendar(2014, 1, 1).getTime(), 10000, 10.0, 2.5);
        Trip t2 = new Trip(2, new GregorianCalendar(2014, 1, 8).getTime(), 10300, 12.5, 3.0);
        Trip t3 = new Trip(3, new GregorianCalendar(2014, 1, 15).getTime(), 10700, 13.0, 3.149);
        mockResolver.insert(FuelTripContract.TripEntry.TRIP_CONTENT_URI, t1.getContentValues());
        mockResolver.insert(FuelTripContract.TripEntry.TRIP_CONTENT_URI, t2.getContentValues());
        mockResolver.insert(FuelTripContract.TripEntry.TRIP_CONTENT_URI, t3.getContentValues());
    }

    // Trip Tests
    public void testTripInsert()
    {
        Trip t1 = new Trip(1, new Date(), 10000, 10.0, 2.5);
        Uri uri = mockResolver.insert(FuelTripContract.TripEntry.TRIP_CONTENT_URI,
            t1.getContentValues());

        assertEquals(1, ContentUris.parseId(uri));
    }

    public void testTripQuery()
    {
        setUpTripData();

        Cursor c = mockResolver.query(FuelTripContract.TripEntry.TRIP_CONTENT_URI, null, null,
            null, null);

        assertEquals(3, c.getCount());
        c.close();
    }

    public void testTripQueryInDescendingOrder()
    {
        setUpTripData();

        String sortOrder = FuelTripContract.TripEntry.COLUMN_NAME_TRIP_DATE
            + " DESC";
        Cursor c = mockResolver.query(FuelTripContract.TripEntry.TRIP_CONTENT_URI, null, null,
            null, sortOrder);

        assertEquals(3, c.getCount());

        c.moveToFirst();
        Trip t = Trip.fromCursor(c);
        assertEquals(3, t.id.intValue());
        assertEquals(new GregorianCalendar(2014, 1, 15).getTime(), t.date);
        assertEquals(10700, t.odometer.intValue());
        assertEquals(13.0, t.volume);
        assertEquals(3.149, t.volumePrice);

        c.moveToNext();
        t = Trip.fromCursor(c);
        assertEquals(2, t.id.intValue());
        assertEquals(new GregorianCalendar(2014, 1, 8).getTime(), t.date);
        assertEquals(10300, t.odometer.intValue());
        assertEquals(12.5, t.volume);
        assertEquals(3.0, t.volumePrice);

        c.moveToNext();
        t = Trip.fromCursor(c);
        assertEquals(1, t.id.intValue());
        assertEquals(new GregorianCalendar(2014, 1, 1).getTime(), t.date);
        assertEquals(10000, t.odometer.intValue());
        assertEquals(10.0, t.volume);
        assertEquals(2.5, t.volumePrice);
        assertTrue(c.isLast());

        c.close();
    }

    public void testTripDelete()
    {
        setUpTripData();
        String where = String.format("%s = %d", FuelTripContract.TripEntry._ID, 1);
        int numDeleted = mockResolver.delete(FuelTripContract.TripEntry.TRIP_CONTENT_URI, where,
            null);

        assertEquals(1, numDeleted);
    }

    // Vehicle Tests
    public void testVehicleInsert()
    {
        Vehicle v1 = new Vehicle("Kia", "Optima");
        Uri uri = mockResolver.insert(FuelTripContract.VehicleEntry.VEHICLE_CONTENT_URI,
            v1.getContentValues());

        assertEquals(1, ContentUris.parseId(uri));
    }
}
