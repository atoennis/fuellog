package com.atoennis.fuellog;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.atoennis.fuellog.NotifyTripDialogFragment.NoticeDialogListener;
import com.atoennis.fuellog.TripDisplayFragment.OnTripDisplayInteractionListener;
import com.atoennis.fuellog.domain.Trip;

public class TripDisplayActivity extends Activity
    implements NoticeDialogListener, OnTripDisplayInteractionListener
{
    private static final String EXTRA_TRIP          = "EXTRA_TRIP";
    private static final String EXTRA_PREVIOUS_TRIP = "EXTRA_PREVIOUS_TRIP";

    private Trip                trip;
    private Trip                previousTrip;

    public static Intent buildTripDisplayActivityIntent(Context context, Trip trip)
    {
        return buildTripDisplayActivityIntent(context, trip, null);
    }

    public static Intent buildTripDisplayActivityIntent(Context context, Trip trip,
        Trip previousTrip)
    {
        Intent intent = new Intent(context, TripDisplayActivity.class);

        Bundle extras = new Bundle();
        extras.putSerializable(EXTRA_TRIP, trip);
        extras.putSerializable(EXTRA_PREVIOUS_TRIP, previousTrip);
        intent.putExtras(extras);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_display);

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            trip = (Trip) extras.getSerializable(EXTRA_TRIP);
            previousTrip = (Trip) extras.getSerializable(EXTRA_PREVIOUS_TRIP);
        }

        if (savedInstanceState != null)
        {
            trip = (Trip) savedInstanceState.getSerializable(EXTRA_TRIP);
            previousTrip = (Trip) savedInstanceState.getSerializable(EXTRA_PREVIOUS_TRIP);
        }
        else
        {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragment_container, TripDisplayFragment.newInstance(trip, previousTrip),
                "PRIMARY_FRAGMENT");
            ft.commit();
        }

        ActionBar ab = getActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.trip_display, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putSerializable(EXTRA_TRIP, trip);
        outState.putSerializable(EXTRA_PREVIOUS_TRIP, previousTrip);

        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                handleUpNavigation();
                return true;
            case R.id.action_discard:
                deleteTrip();
                return true;
            case R.id.action_edit:
                launchTripForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleUpNavigation()
    {
        NavUtils.navigateUpFromSameTask(this);
    }

    private void deleteTrip()
    {
        NotifyTripDialogFragment fragment = NotifyTripDialogFragment.newInstance();
        fragment.show(getFragmentManager(), "DIALOG_FRAGMENT");
    }

    private void launchTripForm()
    {
        Intent intent = TripFormActivity.buildTripFormActivityIntent(this, trip);
        startActivity(intent);
    }

    @Override
    public void onDeleteTrip(DialogFragment dialog)
    {
        dialog.dismiss();

        String where = String.format("%s = %d", FuelTripContract.TripEntry._ID, trip.id);
        getContentResolver().delete(FuelTripContract.TripEntry.TRIP_CONTENT_URI, where, null);
    }

    @Override
    public void onCancel(DialogFragment dialog)
    {
        dialog.getDialog().cancel();
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }
}
