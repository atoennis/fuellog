package com.atoennis.fuellog;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.atoennis.fuellog.domain.Trip;


/**
 * A simple {@link android.app.Fragment} subclass. Activities that contain this fragment must
 * implement the {@link TripDisplayFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link TripDisplayFragment#newInstance} factory method to create an
 * instance of this fragment.
 * 
 */
public class TripDisplayFragment extends Fragment
{
    private static final String              ARG_TRIP                   = "ARG_TRIP";
    private static final String              ARG_PREVIOUS_TRIP          = "ARG_PREVIOUS_TRIP";
    private static final String              FUEL_TRIP_DATE_FORMAT_LONG = "EEEE, MMMM d, yyyy";

    private Trip                             trip;
    private Trip                             previousTrip;
    private OnTripDisplayInteractionListener listener;

    private TextView                         dateDisplay;
    private TextView                         odometerDisplay;
    private TextView                         volumeDisplay;
    private TextView                         volumePriceDisplay;
    private TextView                         distanceDisplay;
    private TextView                         efficiencyDisplay;
    private TextView                         daysDisplay;

    /**
     * Use this factory method to create a new instance of this fragment using the provided
     * parameters.
     * 
     * @param trip Trip to display in a read only state.
     * @return A new instance of fragment TripDisplayFragment.
     */
    public static TripDisplayFragment newInstance(Trip trip)
    {
        return newInstance(trip, null);
    }

    /**
     * Use this factory method to create a new instance of this fragment using the provided
     * parameters.
     * 
     * @param trip Trip to display in a read only state.
     * @param previousTrip Previous trip used to calculate values.
     * @return A new instance of fragment TripDisplayFragment.
     */
    public static TripDisplayFragment newInstance(Trip trip, Trip previousTrip)
    {
        TripDisplayFragment fragment = new TripDisplayFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP, trip);
        args.putSerializable(ARG_PREVIOUS_TRIP, previousTrip);
        fragment.setArguments(args);
        return fragment;
    }

    public TripDisplayFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null)
        {
            trip = (Trip) args.getSerializable(ARG_TRIP);
            previousTrip = (Trip) args.getSerializable(ARG_PREVIOUS_TRIP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_trip_display, container, false);
        dateDisplay = (TextView) view.findViewById(R.id.date_display);
        odometerDisplay = (TextView) view.findViewById(R.id.odometer_display);
        volumeDisplay = (TextView) view.findViewById(R.id.volume_display);
        volumePriceDisplay = (TextView) view.findViewById(R.id.volume_price_display);
        distanceDisplay = (TextView) view.findViewById(R.id.distance_display);
        efficiencyDisplay = (TextView) view.findViewById(R.id.efficiency_display);
        daysDisplay = (TextView) view.findViewById(R.id.days_display);

        // Entered Values
        dateDisplay.setText(new SimpleDateFormat(FUEL_TRIP_DATE_FORMAT_LONG).format(trip.date));
        odometerDisplay.setText(String.format("%,d", trip.odometer));
        volumeDisplay.setText(String.format("%.2f", trip.volume));
        volumePriceDisplay.setText(String.format("%.2f", trip.volumePrice));

        // Calculated Values
        if (previousTrip != null)
        {
            int distance = trip.odometer
                - previousTrip.odometer;
            double gasUsed = trip.volume;
            double efficiency = distance
                / gasUsed;
            int daysBetween = (int) ((trip.date.getTime() - previousTrip.date.getTime()) / (1000 * 60 * 60 * 24));

            distanceDisplay.setText(String.format("+%,d", distance));
            efficiencyDisplay.setText(String.format("%.2f", efficiency));
            daysDisplay.setText(String.format("%d", daysBetween));
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (listener != null)
        {
            listener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            listener = (OnTripDisplayInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(String.format(
                "%s must implement OnTripDisplayInteractionListener", activity.getClass()
                    .getSimpleName()));
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this fragment to allow an
     * interaction in this fragment to be communicated to the activity and potentially other
     * fragments contained in that activity.
     */
    public interface OnTripDisplayInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
