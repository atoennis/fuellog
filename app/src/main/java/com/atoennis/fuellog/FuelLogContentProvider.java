package com.atoennis.fuellog;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class FuelLogContentProvider extends ContentProvider
{
    private static final int        INVALID_URI       = -1;
    private static final String     LOG_TAG           = FuelLogContentProvider.class
                                                          .getSimpleName();
    private static final UriMatcher uriMatcher;
    private static final int        FUEL_TRIP_REQUEST = 0;
    private static final int        VEHICLE_REQUEST   = 1;

    static
    {
        uriMatcher = new UriMatcher(0);
        uriMatcher.addURI(FuelTripContract.AUTHORITY, FuelTripContract.TripEntry.TABLE_NAME,
            FUEL_TRIP_REQUEST);
        uriMatcher.addURI(FuelTripContract.AUTHORITY, FuelTripContract.VehicleEntry.TABLE_NAME,
            VEHICLE_REQUEST);
    }

    private SQLiteOpenHelper        helper;

    public FuelLogContentProvider()
    {
    }

    @Override
    public String getType(Uri uri)
    {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate()
    {
        helper = new FuelTripDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
        String sortOrder)
    {
        SQLiteDatabase db = helper.getWritableDatabase();

        switch (uriMatcher.match(uri))
        {
            case FUEL_TRIP_REQUEST:
                Cursor cursor = db.query(FuelTripContract.TripEntry.TABLE_NAME, projection,
                    selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                return cursor;
            case INVALID_URI:
                throw new IllegalArgumentException(String.format("Query -- Invalid URI: %s", uri));
        }

        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        SQLiteDatabase db = helper.getWritableDatabase();

        switch (uriMatcher.match(uri))
        {
            case FUEL_TRIP_REQUEST:
                long rowId = db.insert(FuelTripContract.TripEntry.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.withAppendedPath(uri, Long.toString(rowId));
            case VEHICLE_REQUEST:
                rowId = db.insert(FuelTripContract.VehicleEntry.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.withAppendedPath(uri, Long.toString(rowId));
            case INVALID_URI:
                throw new IllegalArgumentException(String.format("Insert -- Invalid URI: %s", uri));
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        SQLiteDatabase db = helper.getWritableDatabase();

        switch (uriMatcher.match(uri))
        {
            case FUEL_TRIP_REQUEST:
                getContext().getContentResolver().notifyChange(uri, null);
                return db.update(FuelTripContract.TripEntry.TABLE_NAME, values, selection,
                    selectionArgs);
            case INVALID_URI:
                throw new IllegalArgumentException(String.format("Query -- Invalid URI: %s", uri));
        }
        return -1;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        SQLiteDatabase db = helper.getWritableDatabase();

        switch (uriMatcher.match(uri))
        {
            case FUEL_TRIP_REQUEST:
                int rows = db.delete(FuelTripContract.TripEntry.TABLE_NAME, selection,
                    selectionArgs);

                Log.d(LOG_TAG, String.format("Deleting from uri %s", uri.toString()));
                getContext().getContentResolver().notifyChange(uri, null);

                return rows;
            case INVALID_URI:
                throw new IllegalArgumentException(String.format("Query -- Invalid URI: %s", uri));
        }

        return -1;
    }
}
