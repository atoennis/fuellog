package com.atoennis.fuellog;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.atoennis.fuellog.FuelTripContract.TripEntry;
import com.atoennis.fuellog.FuelTripContract.VehicleEntry;

public class FuelTripDbHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME      = "FuelTrip.db";
    private static final int    DATABASE_VERSION   = 1;

    private static final String TEXT_TYPE          = " TEXT";
    private static final String INTEGER_TYPE       = " INTEGER";
    private static final String COMMA_SEP          = ",";
    private static final String SQL_CREATE_TRIP    = "CREATE TABLE "
                                                       + TripEntry.TABLE_NAME + " ("
                                                       + TripEntry._ID + " INTEGER PRIMARY KEY,"
                                                       + TripEntry.COLUMN_NAME_TRIP_DATE
                                                       + INTEGER_TYPE + COMMA_SEP
                                                       + TripEntry.COLUMN_NAME_TRIP_ODOMETER
                                                       + TEXT_TYPE + COMMA_SEP
                                                       + TripEntry.COLUMN_NAME_TRIP_VOLUME
                                                       + TEXT_TYPE + COMMA_SEP
                                                       + TripEntry.COLUMN_NAME_TRIP_VOLUME_PRICE
                                                       + TEXT_TYPE + " )";
    private static final String SQL_DELETE_TRIP    = "DROP TABLE IF EXISTS "
                                                       + TripEntry.TABLE_NAME;

    private static final String SQL_CREATE_VEHICLE = "CREATE TABLE "
                                                       + VehicleEntry.TABLE_NAME + " ("
                                                       + VehicleEntry._ID + " INTEGER PRIMARY KEY,"
                                                       + VehicleEntry.COLUMN_NAME_VEHICLE_MAKE
                                                       + TEXT_TYPE + COMMA_SEP
                                                       + VehicleEntry.COLUMN_NAME_VEHICLE_MODEL
                                                       + TEXT_TYPE + " )";
    private static final String SQL_DELETE_VEHICLE = "DROP TABLE IF EXISTS "
                                                       + VehicleEntry.TABLE_NAME;


    public FuelTripDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SQL_CREATE_TRIP);
        db.execSQL(SQL_CREATE_VEHICLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL(SQL_DELETE_TRIP);
        db.execSQL(SQL_DELETE_VEHICLE);
        onCreate(db);
    }

}
