package com.atoennis.fuellog.domain;

import android.content.ContentValues;
import android.database.Cursor;

import com.atoennis.fuellog.FuelTripContract;
import com.atoennis.fuellog.FuelTripContract.VehicleEntry;

public class Vehicle
{
    public final String make;
    public final String model;

    public Vehicle()
    {
        this.make = "";
        this.model = "";
    }

    public Vehicle(String make, String model)
    {
        this.make = make;
        this.model = model;
    }

    public ContentValues getContentValues()
    {
        ContentValues values = new ContentValues();
        values.put(VehicleEntry.COLUMN_NAME_VEHICLE_MAKE, make);
        values.put(VehicleEntry.COLUMN_NAME_VEHICLE_MODEL, model);

        return values;
    }

    public static Vehicle fromCursor(Cursor cursor)
    {
        if (cursor != null)
        {
            String make = cursor.getString(cursor
                .getColumnIndex(FuelTripContract.VehicleEntry.COLUMN_NAME_VEHICLE_MAKE));
            String model = cursor.getString(cursor
                .getColumnIndex(FuelTripContract.VehicleEntry.COLUMN_NAME_VEHICLE_MODEL));

            return new Vehicle(make, model);
        }

        return new Vehicle();
    }

}
