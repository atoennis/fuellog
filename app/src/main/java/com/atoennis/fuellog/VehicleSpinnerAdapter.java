package com.atoennis.fuellog;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.atoennis.fuellog.domain.Vehicle;

public class VehicleSpinnerAdapter extends CursorAdapter
{

    public VehicleSpinnerAdapter(Context context)
    {
        super(context, null, false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.vehicle_spinner_item, null);

        TextView model = (TextView) view.findViewById(R.id.model);
        VehicleSpinnerViewHolder holder = new VehicleSpinnerViewHolder(model);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        Vehicle vehicle = Vehicle.fromCursor(cursor);
        VehicleSpinnerViewHolder holder = (VehicleSpinnerViewHolder) view.getTag();

        holder.model.setText(vehicle.model);
    }
    private class VehicleSpinnerViewHolder
    {
        public final TextView model;

        public VehicleSpinnerViewHolder(TextView model)
        {
            this.model = model;
        }
    }

}
